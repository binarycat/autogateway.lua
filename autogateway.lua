#!/bin/lua

-- automatically open non-http links in http

gateway = {
	ipfs = "https://ipfs.io/ipfs/",
	ipns = "https://ipfs.io/ipns/",
	gemini = "https://portal.mozz.us/gemini/",
	gopher = "http://gopher.floodgap.com/gopher/gw?",
}

local scheme, body = string.match(arg[1], "([a-z]+):/?/?(.*)")

os.execute("xdg-open "..string.format("%q", gateway[scheme]..body))

