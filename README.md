# autogateway: automatically open non-http links via http proxy

## install

run `install.sh`

### manual installation
1. place `autogateway.lua` somewhere in `$PATH` (usually `~/.local/bin`)
2. place `autogateway.desktop` in the same directory as `mimeinfo.cache` (usually `~/.local/share/applications`

these paths will be different if you have `$XDG_DATA_HOME` set to a non-default value.

## usage
once the the progam is installed, you can call it directly:

    autogateway.lua gemini://gemini.circumlunar.space

however, the main usecase is being the default program when clicking links in a web browser.

