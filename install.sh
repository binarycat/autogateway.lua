#!/bin/sh -e

cd $(dirname $0)

if [ -z $XDG_DATA_HOME ]
then export XDG_DATA_HOME=$HOME/.local/share
fi

echo installing files...
cp autogateway.desktop $XDG_DATA_HOME/applications
cp autogateway.lua $HOME/.local/bin

echo setting default mime handlers...
for mime in x-scheme-handler/ipfs x-scheme-handler/ipns x-scheme-handler/gemini x-scheme-handler/gopher
do
	# only install autogateway as the default if no default is set
	if [ -z $(xdg-mime query default $mime) ]
	then
		echo setting as default for $mime
		xdg-mime default autogateway.desktop $mime
	fi
done
